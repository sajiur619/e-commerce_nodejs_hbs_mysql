var session = require('express-session');
const Admin = require("../models/admin");



function auth(req, res, next) {
    if (req.session.admin) {
        console.log(req.session.admin);
        next();
    } else {
        Admin.findOne({ where: { email: req.body.email, password: req.body.password } })
            .then(admin => {
                if (admin) {
                    req.session.admin = admin;
                    next();
                } else {
                    res.redirect('/admin-login');
                }
            })
            .catch(err => res.redirect('/admin-login'));

    }
}

module.exports = auth;