const express = require('express');

const router = express.Router();


router.get('', async(req, res) => {
    res.render('home/home', { layout: 'home-layout' });
})

module.exports = router;