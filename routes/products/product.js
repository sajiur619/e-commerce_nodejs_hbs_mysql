const express = require("express");
const Sequelize = require("sequelize");
const Product = require('../../models/Product');
const auth = require('../../middleware/auth');
const router = express.Router();
const Op = Sequelize.Op;
//route



router.get('/addproduct', auth, async(req, res) => {
    res.render('products/addProducts', {
        layout: 'dash-layout'
    });
})

// sign up here
router.post("/addproduct", (req, res) => {
    let {
        name,
        password,
        email
    } = req.body;
    let errors = [];

    // Validate Fields
    if (!name) {
        errors.push({ text: "Please add a name" });
    }

    if (!password) {
        errors.push({ text: "Please add image" });
    }

    if (!email) {
        errors.push({ text: "Please add some sex" });
    }


    // Insert into table
    Product.create({
            name,
            password,
            email

        })
        .then((product) => res.redirect("/admin/product/addproduct"))
        .catch((err) => res.render("error", { error: err.message }));

});
module.exports = router;