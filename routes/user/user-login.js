const express = require('express');
const User = require('../../models/user');
// const auth = require('../../middleware/auth');
const router = express.Router();
var session = require('express-session');

router.get('/', async(req, res) => {
    if (req.session.user) {
        // res.render('admin/admin', {layout: false, user:req.session.user});
        res.redirect('/user-login');
    } else {
        res.render('user/user-login', {
            layout: 'home-layout'
        });
    }
});

router.post('/', async(req, res) => {
    // res.render('admin/admin', {layout: false});
    res.redirect('/user-Account');

});

router.get('/out', async(req, res) => {
    req.session.destroy(function() {
        console.log("user logged out.")
    });

    res.redirect('/user-login');

});
module.exports = router;