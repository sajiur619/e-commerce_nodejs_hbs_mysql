const express = require("express");
const User = require("../../models/user");
const Sequelize = require("sequelize");
const auth = require('../../middleware/auth');
const router = express.Router();
const Op = Sequelize.Op;


//contacts route
router.get("/", auth, async(req, res) => {
    User.findAll()
        .then((users) =>

            res.render("user/user-table", {
                users,
                layout: 'dash-layout'
            })
        )
        .catch((err) => res.render("error", { error: err }));

    // Display contact form
});
module.exports = router;