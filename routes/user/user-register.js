const express = require("express");
const Sequelize = require("sequelize");
const User = require('../../models/user');
// const auth = require('../../middleware/auth');
const router = express.Router();
var session = require('express-session');
const Op = Sequelize.Op;
//route
router.get('/', async(req, res) => {
    res.render('user/user-form', {
        layout: 'home-layout'
    });
})

// sign up here
router.post("/", (req, res) => {
    let {
        name,
        password,
        email,
        phone
    } = req.body;
    let errors = [];

    // Validate Fields
    if (!name) {
        errors.push({ text: "Please add a name" });
    }

    if (!password) {
        errors.push({ text: "Please add password" });
    }

    if (!email) {
        errors.push({ text: "Please add email" });
    }
     if (!phone) {
        errors.push({ text: "Please add phone number" });
    }


    // Insert into table
    User.create({
            name,
            password,
            email,
            phone

        })
        .then((user) => res.redirect("/user-login"))
        .catch((err) => res.render("error", { error: err.message }));

});
module.exports = router;