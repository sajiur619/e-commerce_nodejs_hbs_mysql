const Sequelize = require('sequelize');
const db = require('../config/database');

const Product = db.define('product', {
    name: {
        type: Sequelize.STRING
    },
    email: {
        type: Sequelize.STRING
    },
    password: {
        type: Sequelize.STRING
    },

});

Product.sync().then(() => {
    console.log('Product table created');
});
module.exports = Product;